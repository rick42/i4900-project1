#include "ske.h"
#include "prf.h"
#include <openssl/sha.h>
#include <openssl/evp.h>
#include <openssl/hmac.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* memcpy */
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#ifdef LINUX
#define MMAP_SEQ MAP_PRIVATE|MAP_POPULATE
#else
#define MMAP_SEQ MAP_PRIVATE
#endif

/* NOTE: since we use counter mode, we don't need padding, as the
 * ciphertext length will be the same as that of the plaintext.
 * Here's the message format we'll use for the ciphertext:
 * +------------+--------------------+----------------------------------+
 * | 16 byte IV | C = AES(plaintext) | HMAC(IV|C) (32 bytes for SHA256) |
 * +------------+--------------------+----------------------------------+
 * */

/* we'll use hmac with sha256, which produces 32 byte output */
#define HM_LEN 32
#define KDF_KEY "qVHqkOVJLb7EolR9dsAMVwH1hRCYVx#I"
/* need to make sure KDF is orthogonal to other hash functions, like
 * the one used in the KDF, so we use hmac with a key. */

int ske_keyGen(SKE_KEY* K, unsigned char* entropy, size_t entLen)
{
	unsigned char* buf;
	if(entropy){
		buf = malloc(entLen + HM_LEN);

		// concatenate KDF_KEY and entropy
		for (int i = 0; i < HM_LEN; i++){
			buf[i] = KDF_KEY[i];
		}
		for (int i = 0; i < entLen; i++){
			buf[HM_LEN + i] = entropy[i]; 
		}

		// set the concatenation result as the seed
		setSeed(buf, entLen + HM_LEN);
		free(buf);
	}
	// generate aes and hmac keys
	buf = malloc(2 * HM_LEN);
	randBytes(buf, 2 * HM_LEN);

	for(int i = 0; i < HM_LEN; i++){
		K->aesKey[i] = buf[i];
	}
	for(int i = 0; i < HM_LEN; i++){
		K->hmacKey[i] = buf[HM_LEN+i];
	}

	free(buf);
	return 0;
}
size_t ske_getOutputLen(size_t inputLen)
{
	return AES_BLOCK_SIZE + inputLen + HM_LEN;
}
size_t ske_encrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		SKE_KEY* K, unsigned char* IV)
{
	size_t nWritten = 0;

	// first write IV
	for(int i = 0; i < AES_BLOCK_SIZE; i++){
		outBuf[i] = IV[i];
	}

	// second write C
	EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
	if (1!=EVP_EncryptInit_ex(ctx,EVP_aes_256_ctr(),0,K->aesKey,IV))
		ERR_print_errors_fp(stderr);
	if (1!=EVP_EncryptUpdate(ctx,outBuf+AES_BLOCK_SIZE,&nWritten,inBuf,len))
		ERR_print_errors_fp(stderr);
	EVP_CIPHER_CTX_free(ctx);

	// add bytes from IV
	nWritten += AES_BLOCK_SIZE;

	for(int i = nWritten; i < nWritten + 32; i++){
		outBuf[i] = 0;
	}

	// third write HMAC
	HMAC(EVP_sha256(), K->hmacKey, HM_LEN, outBuf, nWritten, outBuf+nWritten, NULL);

	nWritten += HM_LEN;
	return nWritten;
}
size_t ske_encrypt_file(const char* fnout, const char* fnin,
		SKE_KEY* K, unsigned char* IV, size_t offset_out)
{
	int fd_in = open(fnin, O_RDONLY);
	if(fd_in < 0){
        printf("\n\"%s \" could not open\n", fnin);
        exit(1);
    }

    struct stat statbufIn;
    int err = fstat(fd_in, &statbufIn);
    if(err < 0){
        printf("\n\"%s \" could not open\n",fnin);
        exit(2);
    }

    size_t lenIn = statbufIn.st_size;
	unsigned char* inMap = mmap(NULL, lenIn, PROT_READ, MAP_PRIVATE,
		                        fd_in, 0);

	int fd_out = open(fnout, O_RDWR | O_CREAT, (mode_t)0600);
	if(fd_out < 0){
        printf("\n\"%s \" could not open\n", fnout);
        exit(3);
    }

    size_t lenOut = AES_BLOCK_SIZE + lenIn + HM_LEN + offset_out;
    ftruncate(fd_out, lenOut);

    unsigned char* outMap = mmap(0, lenOut, PROT_READ | PROT_WRITE,
    							 MAP_SHARED, fd_out, offset_out);

	return ske_encrypt(outMap, inMap, lenIn, K, IV);
}
size_t ske_decrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		SKE_KEY* K)
{
	int cLen = len - AES_BLOCK_SIZE - HM_LEN;
	unsigned char IV[AES_BLOCK_SIZE];
	unsigned char C[cLen];
	unsigned char HM[HM_LEN];

	// parse message components
	for(int i = 0; i < AES_BLOCK_SIZE; i++){
		IV[i] = inBuf[i];
	}
	for(int i = 0; i < cLen; i++){
		C[i] = inBuf[i+AES_BLOCK_SIZE];
	}
	for(int i = 0; i < HM_LEN; i++){
		HM[i] = inBuf[i+len-HM_LEN];
	}

	// check validity
	unsigned char* buf = malloc(HM_LEN);
	HMAC(EVP_sha256(), K->hmacKey, HM_LEN, inBuf, len-HM_LEN, buf, NULL);

	for(int i = 0; i < HM_LEN; i++){
		if(HM[i] != buf[i]){
			return -1;
		}
	}
	free(buf);

	// decrypt
	int nWritten = 0;
	EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
	if (1!=EVP_DecryptInit_ex(ctx,EVP_aes_256_ctr(),0,K->aesKey,IV))
		ERR_print_errors_fp(stderr);
	if (1!=EVP_DecryptUpdate(ctx,outBuf,&nWritten,C,cLen))
		ERR_print_errors_fp(stderr);

	EVP_CIPHER_CTX_free(ctx);
	return cLen;
}
size_t ske_decrypt_file(const char* fnout, const char* fnin,
		SKE_KEY* K, size_t offset_in)
{
	int fd_in = open(fnin, O_RDONLY);
	if(fd_in < 0){
        printf("\n\"%s \" could not open\n", fnin);
        exit(1);
    }

    struct stat statbufIn;
    int err = fstat(fd_in, &statbufIn);
    if(err < 0){
        printf("\n\"%s \" could not open\n",fnin);
        exit(2);
    }

    size_t lenIn = statbufIn.st_size;
	unsigned char* inMap = mmap(NULL, lenIn, PROT_READ, MAP_PRIVATE,
		                        fd_in, offset_in);

	int fd_out = open(fnout, O_RDWR | O_CREAT, (mode_t)0600);
	if(fd_out < 0){
        printf("\n\"%s \" could not open\n", fnout);
        exit(3);
    }

    size_t lenOut = lenIn - AES_BLOCK_SIZE - HM_LEN;
    ftruncate(fd_out, lenOut);

    unsigned char* outMap = mmap(0, lenOut, PROT_READ | PROT_WRITE,
                                 MAP_SHARED, fd_out, 0);

	return ske_decrypt(outMap, inMap, lenIn, K);
}
